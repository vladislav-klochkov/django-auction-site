# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-06-08 19:58+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: Auction/settings.py:123
msgid "English"
msgstr "Английский"

#: Auction/settings.py:124
msgid "Russian"
msgstr "Русский"

#: AuctionApp/forms.py:9
msgid "Username"
msgstr "Имя пользователя"

#: AuctionApp/forms.py:10
msgid "Email"
msgstr "Почта"

#: AuctionApp/forms.py:11
msgid "Password"
msgstr "Пароль"

#: AuctionApp/forms.py:12
msgid "Password again"
msgstr "Пароль еще раз"

#: AuctionApp/forms.py:31
msgid "Enter date in form dd.mm.yyy hh:mm"
msgstr "Введите дату в формате дд.мм.гггг чч:мм"

#: AuctionApp/views.py:36
msgid "You must be logged in to use this feature."
msgstr "Вы должны быть авторизованы, чтобы использовать это."

#: AuctionApp/views.py:51
msgid "New passwords don't match"
msgstr "Пароли не совпадают"

#: AuctionApp/views.py:71
msgid "New User is created. Please Login"
msgstr "Новый пользователь создан. Пожалуйста Войдите в систему"

#: AuctionApp/views.py:138
msgid "New Auction has been saved"
msgstr "Новый аукцион был сохранен"

#: AuctionApp/views.py:191 AuctionApp/views.py:207 AuctionApp/views.py:251
msgid "Your requested auction is not Active!!"
msgstr "Вы запрашиваете аукцион, который неактивен!!"

#: AuctionApp/views.py:204
msgid "You can't edit an auction you're not selling."
msgstr "Вы не можете изменять не свой аукцион."

#: AuctionApp/views.py:214
msgid "Description edited"
msgstr "Описание изменено"

#: AuctionApp/views.py:233
msgid "You can't bid on an auction you're selling."
msgstr "Вы не можете поставить на собственный аукцион."

#: AuctionApp/views.py:241
msgid "You already have the highest bid, you can't bid higher!"
msgstr "У вас уже наивысшая ставка, вы не можете поставить больше!"

#: AuctionApp/views.py:269
msgid "The seller has changed description. Please bid again."
msgstr "Продавец изменить описание. Пожалуйста, сделайте ставку еще раз."

#: AuctionApp/views.py:280
msgid ""
"You must have a . separator and no more than two decimals, example: '1.43'"
msgstr ""
"Число должно иметь . (точку) и не более двую цифр после нее, например: '1.43'"

#: AuctionApp/views.py:326
msgid "bid registered"
msgstr "Ставка зарегистрирована"

#: AuctionApp/views.py:330
msgid ""
"The new bid must be at least 0.01 more than the current bid or minimum bid"
msgstr ""
"Ставка должна быть как минимум на 0.01 больше, чем текущая ставки или "
"минимальная ставка"

#: AuctionApp/views.py:352
msgid "Currency changed to: {}"
msgstr "Валюта изменена на: {}"

#: templates/base.html:17
msgid "Yet Another Auction Site"
msgstr "Супер Аукцион"

#: templates/base.html:19
msgid "Home"
msgstr "Главная"

#: templates/base.html:20 templates/change_language.html:6
msgid "Change Language"
msgstr "Изменить язык"

#: templates/base.html:21 templates/change_currency.html:6
msgid "Change Currency"
msgstr "Изменить валюту"

#: templates/base.html:24
msgid "Create Auction"
msgstr "Создать аукцион"

#: templates/base.html:26
msgid "Logout"
msgstr "Выйти"

#: templates/base.html:28
msgid "Log in"
msgstr "Войти"

#: templates/bid_auction.html:12 templates/browse_auction.html:21
msgid "Bid"
msgstr "Поставить"

#: templates/bid_auction.html:15
msgid "Description: "
msgstr "Описание: "

#: templates/bid_auction.html:18 templates/browse.html:32
msgid "Seller: "
msgstr "Продавец: "

#: templates/bid_auction.html:21
msgid "Minimum bid: "
msgstr "Минимальная ставка"

#: templates/bid_auction.html:23 templates/browse_auction.html:34
msgid "Current bid"
msgstr "Текущая ставка"

#: templates/bid_auction.html:26
msgid "Auction deadline"
msgstr "Аукцион заканчивается"

#: templates/bid_auction.html:29
msgid "Please note that you are bidding in currency"
msgstr "Пожалуйста, проверьте выбранную валюту"

#: templates/browse.html:13
msgid "Search for auctions"
msgstr "Поиск аукциона"

#: templates/browse.html:16
msgid "Search"
msgstr "Найти"

#: templates/browse.html:21
msgid "Current auctions: "
msgstr "Текущие аукционы: "

#: templates/browse.html:33
msgid "State: "
msgstr "Состояние: "

#: templates/browse.html:34
msgid "Auction deadline: "
msgstr "Аукцион заканчивается: "

#: templates/browse.html:37
msgid "Details"
msgstr "Подробнее"

#: templates/browse_auction.html:11
msgid "Edit Description"
msgstr "Изменить Описание"

#: templates/browse_auction.html:16
msgid "Ban"
msgstr "Забанить"

#: templates/browse_auction.html:26
msgid "Description"
msgstr "Описание"

#: templates/browse_auction.html:29 templates/edit_auction.html:15
msgid "Seller"
msgstr "Продавец"

#: templates/browse_auction.html:32
msgid "Minimum bid"
msgstr "Минимальная ставка"

#: templates/browse_auction.html:37 templates/edit_auction.html:19
msgid "Auction closes"
msgstr "Аукцион закрывается"

#: templates/browse_auction.html:40
msgid "Prices shown in"
msgstr "Цены показываются в"

#: templates/confirmation.html:4
msgid "Are you sure you want to add the Auction?"
msgstr "Вы уверены, что хотите создать аукцион?"

#: templates/confirmation.html:13
msgid "Title: "
msgstr "Название: "

#: templates/confirmation.html:23
msgid "Yes"
msgstr "Да"

#: templates/confirmation.html:27
msgid "No"
msgstr "Нет"

#: templates/create_auction.html:4
msgid "Create new Auction"
msgstr "Создать новый аукцион"

#: templates/create_auction.html:15
msgid "Create"
msgstr "Создать"

#: templates/create_auction.html:17
msgid "Please note that you are using currency"
msgstr "Пожалуйста, проверьте выбранную валюту"

#: templates/edit_auction.html:4
msgid "Edit auction "
msgstr "Изменить аукцион "

#: templates/edit_auction.html:12
msgid "Save"
msgstr "Сохранить"

#: templates/index.html:4
msgid "Home Page"
msgstr "Главная страница"

#: templates/index.html:8
msgid "logged in as "
msgstr "Вы вошли как "

#: templates/registration/login.html:4 templates/registration/login.html:22
msgid "Login"
msgstr "Войти"

#: templates/registration/login.html:26
msgid "Don't have an account?"
msgstr "Нету аккаунта?"

#: templates/registration/login.html:27
msgid "Create here for free"
msgstr "Создать здесь бесплатно"

#: templates/registration/register.html:4
msgid "Create user"
msgstr "Создать пользователя"

#: templates/registration/register.html:11
msgid "Register"
msgstr "Зарегистрироваться"

#: templates/view_profile.html:6
msgid "'s profile"
msgstr " профиль"

#: templates/view_profile.html:14
msgid "Change Email address"
msgstr "Изменить почту"

#: templates/view_profile.html:17
msgid "Old email: "
msgstr "Стараяя почта: "

#: templates/view_profile.html:20
msgid "New Email: "
msgstr "Новая почта: "

#: templates/view_profile.html:26
msgid "Change Email"
msgstr "Изменить почту"

#: templates/view_profile.html:32 templates/view_profile.html:46
msgid "Change Password"
msgstr "Изменить пароль"

#: templates/view_profile.html:36
msgid "New Password: "
msgstr "Новый пароль: "

#: templates/view_profile.html:40
msgid "New Password again: "
msgstr "Пароль еще раз: "
